require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const indexRouter = require("./app/routes/index");
const rolesRouter = require("./app/routes/roles");
const usersRouter = require("./app/routes/users");
const authRouter = require("./app/routes/auth");
const errorController = require("./app/controllers/error");
const app = express();
const path = require("path");
const { I18n, getLocale } = require("i18n");
const i18n = new I18n();
i18n.configure({
  locales: ["en", "id"],
  directory: path.resolve("locales"),
  objectNotation: true,
  defaultLocale: process.env.APP_LANGUAGE || "en",
  directoryPermissions: "755",
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});
app.use(i18n.init);
app.use((req, res, next) => {
  const clientLocale = req.get("locale");
  if (clientLocale) {
    i18n.setLocale(req, clientLocale);
  }
  next();
});
app.use(indexRouter);
app.use("/roles", rolesRouter);
app.use("/users", usersRouter);
app.use("/auth", authRouter);
app.use(errorController.get404);

app.use((error, req, res, next) => {
  //console.log(error);
  const status = error.statusCode || 500;
  const message = error.message;
  const errors = error.errors;
  const data = error.data;
  res.status(status).json({ message: message, data: data, errors: errors });
});

app.listen(process.env.PORT, () => {
  console.log(`listening port :::${process.env.PORT}:::`);
});
