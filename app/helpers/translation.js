require("dotenv").config();
const path = require("path");
const { I18n, getLocale } = require("i18n");
const i18n = new I18n();
i18n.configure({
  locales: ["en", "id"],
  directory: path.resolve("locales"),
  objectNotation: true,
  defaultLocale: process.env.APP_LANGUAGE || "en",
  directoryPermissions: "755",
});

const translate = (text, val = []) => {
  // // let clientLocale = "id";
  const client = global.clientLocale;
  if (client) {
    i18n.setLocale(client);
  }
  // console.log(typeof client);

  const content = i18n.__(text, ...val);

  return content;
};

module.exports = translate;
