require("dotenv").config();
const t = require("../helpers/translation");
const email = require("../configs/emails");
const fs = require("fs");
const ejs = require("ejs");
const templateVerifyEmail = fs.readFileSync(
  "./app/views/email/verifyEmail.ejs",
  "utf8"
);

const from_email = process.env.MAIL_FROM_EMAIL;
const from_name = process.env.MAIL_FROM_NAME;

const emailVerification = async (payload) => {
  if (!payload) {
    return false;
  }
  const email_to = payload.email;
  const name_to = payload.firstName;
  const from = from_name + " " + from_email;
  const subject = t("verifyEmail", [name_to]);
  const linkVerify =
    process.env.EMAIL_CONFIRMATION_URL + "?token=" + payload.emailToken;
  payload["pageTitle"] = t("templateEmail.verification.title");
  payload["descriptionTop"] = t("templateEmail.verification.descriptionTop");
  payload["btnVerify"] = t("templateEmail.verification.btnVerify");
  payload["linkVerify"] = linkVerify;
  payload["descriptionBottom"] = t(
    "templateEmail.verification.descriptionBottom"
  );

  const content = ejs.render(templateVerifyEmail, payload);
  return email(from, email_to, subject, content);
};

module.exports = emailVerification;
