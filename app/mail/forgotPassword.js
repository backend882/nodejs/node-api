require("dotenv").config();
const t = require("../helpers/translation");
const email = require("../configs/emails");
const fs = require("fs");
const ejs = require("ejs");
const templateResetEmail = fs.readFileSync(
  "./app/views/email/forgotPassword.ejs",
  "utf8"
);
const from_email = process.env.MAIL_FROM_EMAIL;
const from_name = process.env.MAIL_FROM_NAME;
const forgotPassword = async (payload) => {
  if (!payload) {
    return false;
  }
  const name_to = payload.firstName;
  const email_to = name_to + " " + payload.email;
  const from = from_name + " " + from_email;
  const subject = t("templateEmail.forgotPassword.subject");
  const linkReset =
    process.env.EMAIL_FORGOT_PASSOWRD_URL + "?token=" + payload.emailToken;
  payload["pageTitle"] = t("templateEmail.forgotPassword.subject");
  payload["descriptionTop"] = t("templateEmail.forgotPassword.body1");
  payload["descriptionSub"] = t("templateEmail.forgotPassword.body2");
  payload["btnReset"] = t("templateEmail.forgotPassword.btnReset");
  payload["linkReset"] = linkReset;
  payload["descriptionBottom"] = t(
    "templateEmail.verification.descriptionBottom"
  );
  const content = ejs.render(templateResetEmail, payload);
  return email(from, email_to, subject, content);
};
module.exports = forgotPassword;
