const fs = require("fs");
require("dotenv").config();
const nodemailer = require("nodemailer");

const sendEmail = async (from, to, subject, content) => {
  const host = process.env.MAIL_HOST;
  const port = process.env.MAIL_PORT;
  const user = process.env.MAIL_USERNAME;
  const password = process.env.MAIL_PASSOWRD;

  const transporter = nodemailer.createTransport({
    host: host,
    port: port,
    auth: {
      user: user,
      pass: password,
    },
  });

  const message = {
    from: from,
    to: to,
    subject: subject,
    html: content,
  };

  const send = await transporter
    .sendMail(message)
    .catch((err) => console.log(err));
  return send;
};

module.exports = sendEmail;
