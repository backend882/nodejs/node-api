const model = require("../models");
const Option = model.option;

exports.option = (key) => {
  let value = "";
  return Option.findAll({ where: { key: key } }).then((data) => {
    value = data.value;
    return value;
  });
};
