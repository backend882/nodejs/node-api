const { body, validationResult } = require("express-validator");
const model = require("../../models");

const User = model.user;

const validator = [
  body("token")
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage((value, { req, res }) => {
      return req.__("error.form.required", req.__("token"));
    })
    .custom(async (value, { req, res }) => {
      const users = await User.findOne({ where: { emailToken: value } });
      if (users === null) {
        return Promise.reject(req.__("error.notFound", req.__("user")));
      }
    })
    .bail(),
  body("password")
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage((value, { req, res }) => {
      return req.__("error.form.required", req.__("password"));
    })
    .bail()
    .isLength({ min: 5 })
    .withMessage((value, { req, res }) => {
      return req.__("error.form.min", req.__("password"), 5);
    })
    .bail(),
  body("confirmPassword")
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage((value, { req, res }) => {
      return req.__("error.form.required", req.__("confirmPassword"));
    })
    .bail()
    .custom((value, { req }) => {
      if (value !== req.body.password) {
        return Promise.reject(
          req.__("error.form.notMatch", req.__("confirmPassword"))
        );
      } else {
        return value;
      }
    })
    .bail(),
];

const reporter = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const errorMessages = req.__("error.fail", req.__("validation"));
    const error = new Error(errorMessages);
    error.statusCode = 400;
    error.errors = errors.errors;
    throw error;
  }
  next();
};
module.exports = { resetPassword: [validator, reporter] };
