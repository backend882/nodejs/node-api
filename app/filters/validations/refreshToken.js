const { body, validationResult } = require("express-validator");

// exports.signup = [
const validator = [
  body("token")
    .trim()
    .not()
    .isEmpty()
    .withMessage((value, { req, res }) => {
      return req.__("error.form.required", req.__("refreshToken"));
    })
    .bail(),
];

const reporter = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const errorMessages = req.__("error.fail", req.__("validation"));
    const error = new Error(errorMessages);

    error.statusCode = 400;
    error.errors = errors.errors;
    throw error;
  }
  next();
};
module.exports = { refreshToken: [validator, reporter] };
