const { body, validationResult } = require("express-validator");

const validator = [
  body("oldPassword")
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage((value, { req, res }) => {
      return req.__("error.form.required", req.__("oldPassword"));
    })
    .bail()
    .isLength({ min: 5 })
    .withMessage((value, { req, res }) => {
      return req.__("error.form.min", req.__("oldPassword"), 5);
    })
    .bail(),
  body("newPassword")
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage((value, { req, res }) => {
      return req.__("error.form.required", req.__("newPassword"));
    })
    .bail()
    .isLength({ min: 5 })
    .withMessage((value, { req, res }) => {
      return req.__("error.form.min", req.__("newPassword"), 5);
    })
    .bail(),
  body("confirmPassword")
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage((value, { req, res }) => {
      return req.__("error.form.required", req.__("confirmPassword"));
    })
    .bail()
    .custom((value, { req }) => {
      if (value !== req.body.newPassword) {
        return Promise.reject(
          req.__("error.form.notMatch", req.__("confirmPassword"))
        );
      } else {
        return value;
      }
    })
    .bail(),
];

const reporter = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const errorMessages = req.__("error.fail", req.__("validation"));
    const error = new Error(errorMessages);
    error.statusCode = 400;
    error.errors = errors.errors;
    throw error;
  }
  next();
};
module.exports = { updatePassword: [validator, reporter] };
