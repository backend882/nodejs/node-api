const { body, validationResult } = require("express-validator");
const model = require("../../models");

const User = model.user;
const validator = [
  body("email")
    .trim()
    .not()
    .isEmpty()
    .withMessage((value, { req, res }) => {
      return req.__("error.form.required", req.__("email"));
    })
    .bail()
    .isEmail()
    .withMessage((value, { req, res }) => {
      return req.__("error.form.invalid", req.__("email"));
    })
    .custom(async (value, { req }) => {
      const users = await User.findOne({ where: { email: value } });
      if (users === null) {
        return Promise.reject(req.__("error.notFound", req.__("email")));
      }
    })
    .bail(),
  body("password")
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage((value, { req, res }) => {
      return req.__("error.form.required", req.__("password"));
    })
    .bail()
    .isLength({ min: 5 })
    .withMessage((value, { req, res }) => {
      return req.__("error.form.min", req.__("password"), 5);
    })

    .bail(),
];

const reporter = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const errorMessages = req.__("error.fail", req.__("validation"));
    const error = new Error(errorMessages);

    error.statusCode = 400;
    error.errors = errors.errors;
    throw error;
  }
  next();
};
module.exports = { signin: [validator, reporter] };
