const { body, validationResult } = require("express-validator");
const model = require("../../../models");
const Role = model.role;

const validator = [
  body("name")
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage((value, { req, res }) => {
      return req.__("error.form.required", req.__("name"));
    })
    .bail()
    .isLength({ min: 3 })
    .withMessage((value, { req, res }) => {
      return req.__("error.form.min", req.__("name"), 3);
    })
    .bail()
    .custom(async (value, { req }) => {
      const roleId = req.params.roleId;
      const roles = await Role.findOne({
        where: { name: value },
        paranoid: false,
      });
      if (roles !== null && roles.id !== Number(roleId)) {
        return Promise.reject(req.__("error.form.duplicate", req.__("role")));
      }
    })
    .bail(),
];

const reporter = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const errorMessages = req.__("error.fail", req.__("validation"));
    const error = new Error(errorMessages);

    error.statusCode = 400;
    error.errors = errors.errors;
    throw error;
  }
  next();
};
module.exports = { updateRole: [validator, reporter] };
