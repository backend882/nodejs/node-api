const { body, validationResult } = require("express-validator");
const model = require("../../../models");

const User = model.user;
const validator = [
  body("firstName")
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage((value, { req, res }) => {
      return req.__("error.form.required", req.__("firstName"));
    })
    .bail()
    .isLength({ min: 3 })
    .withMessage((value, { req, res }) => {
      return req.__("error.form.min", req.__("firstName"), 3);
    })
    .bail(),
  body("email")
    .trim()
    .not()
    .isEmpty()
    .withMessage((value, { req, res }) => {
      return req.__("error.form.required", req.__("email"));
    })
    .bail()
    .isEmail()
    .withMessage((value, { req, res }) => {
      return req.__("error.form.invalid", req.__("email"));
    })
    .custom(async (value, { req }) => {
      const userId = req.params.userId;
      const users = await User.findOne({
        where: { email: value },
        paranoid: false,
      });
      if (users !== null && users.id !== Number(userId)) {
        return Promise.reject(req.__("error.form.duplicate", req.__("email")));
      }
    })
    .bail(),
  body("password")
    .custom(async (value, { req }) => {
      if (value && value.length < 5) {
        return Promise.reject(req.__("error.form.min", req.__("password"), 5));
      }
    })
    .bail(),
  body("role")
    .trim()
    .not()
    .isEmpty()
    .withMessage((value, { req, res }) => {
      return req.__("error.form.required", req.__("role"));
    })
    .bail(),
];

const reporter = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const errorMessages = req.__("error.fail", req.__("validation"));
    const error = new Error(errorMessages);

    error.statusCode = 400;
    error.errors = errors.errors;
    throw error;
  }
  next();
};
module.exports = { updateUser: [validator, reporter] };
