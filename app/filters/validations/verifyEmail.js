const { body, validationResult } = require("express-validator");
const model = require("../../models");

const User = model.user;
// exports.signup = [
const validator = [
  body("token")
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage((value, { req, res }) => {
      return req.__("error.form.required", req.__("token"));
    })
    .custom(async (value, { req }) => {
      const users = await User.findOne({ where: { emailToken: value } });
      if (users === null) {
        return Promise.reject(req.__("error.notFound", req.__("user")));
      }
    })
    .bail(),
];

const reporter = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const errorMessages = req.__("error.fail", req.__("validation"));
    const error = new Error(errorMessages);

    error.statusCode = 400;
    error.errors = errors.errors;
    throw error;
  }
  next();
};
module.exports = { verifyEmail: [validator, reporter] };
