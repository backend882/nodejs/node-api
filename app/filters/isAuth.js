const jwt = require("jsonwebtoken");
const models = require("../models");
const Users = models.user;
const Roles = models.role;

module.exports = async (req, res, next) => {
  const urlPath = req.url.match("^[^?]*")[0].split("/").slice(1)[0];
  const authorization = req.get("Authorization");
  if (!authorization) {
    const error = new Error(req.__("notAuthenticated"));
    error.statusCode = 401;
    return next(error);
  }
  const tokenSplit = authorization.split(" ");
  if (tokenSplit[0] !== "Bearer" || !tokenSplit[0]) {
    const error = new Error(req.__("notAuthenticated"));
    error.statusCode = 401;
    return next(error);
  }
  token = tokenSplit[1];
  if (!token) {
    const error = new Error(req.__("notAuthenticated"));
    error.statusCode = 401;
    return next(error);
  }
  let decodeToken;

  try {
    if (urlPath === "refresh-token") {
      decodeToken = jwt.decode(token, process.env.PRIVATE_KEY);
    } else {
      decodeToken = jwt.verify(token, process.env.PRIVATE_KEY);
    }
  } catch (err) {
    const error = new Error(req.__("notAuthenticated"));
    error.statusCode = 401;
    return next(error);
  }
  if (!decodeToken) {
    const error = new Error(req.__("notAuthenticated"));
    error.statusCode = 401;
    return next(error);
  }
  const userId = decodeToken.userId;

  const dataUsers = await Users.findOne({
    where: { id: userId, verified: 1, status: 1 },
    include: Roles,
  });
  if (!dataUsers) {
    const error = new Error(req.__("notAuthenticated"));
    error.statusCode = 401;
    return next(error);
  }
  req.userId = userId;
  req.userLogin = dataUsers;
  next();
};
