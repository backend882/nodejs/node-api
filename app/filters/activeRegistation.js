const model = require("../models");
const Option = model.option;

module.exports = async (req, res, next) => {
  const registration = await Option.findOne({ where: { key: "registration" } });
  if (registration === null || registration.value !== "1") {
    const error = new Error(req.__("error.notFound", req.__("url")));
    error.statusCode = 404;
    next(error);
  }
  next();
};
