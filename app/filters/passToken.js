const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  const authorization = req.get("Authorization");
  if (!authorization) {
    const error = new Error(req.__("notAuthenticated"));
    error.statusCode = 401;
    throw error;
  }
  const tokenSplit = authorization.split(" ");
  if (tokenSplit[0] !== "Bearer" || !tokenSplit[0]) {
    const error = new Error(req.__("notAuthenticated"));
    error.statusCode = 401;
    throw error;
  }
  token = tokenSplit[1];
  if (!token) {
    const error = new Error(req.__("notAuthenticated"));
    error.statusCode = 401;
    throw error;
  }
  let decodeToken;
  try {
    decodeToken = jwt.decode(token, process.env.PRIVATE_KEY);
  } catch (err) {
    err.statusCode = 500;
    throw err;
  }
  if (!decodeToken) {
    const error = new Error(req.__("notAuthenticated"));
    error.statusCode = 401;
    throw error;
  }
  next();
};
