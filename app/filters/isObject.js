module.exports = (object) => {
  if (!object.match(/^[0-9a-fA-F]{24}$/)) {
    const error = new Error(req.__("error.notFound", req.__("data")));
    error.statusCode = 404;
    throw error;
  }
  return true;
};
