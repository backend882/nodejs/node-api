const model = require("../models");
const users = model.user;
const roles = model.role;

module.exports = async (req, res, next) => {
  const userId = req.userId;
  const userData = await users.findOne({
    where: { id: userId },
    include: roles,
  });
  if (userData === null) {
    const error = new Error(req.__("accessDenied"));
    error.statusCode = 401;
    next(error);
  } else {
    if (userData.role) {
      if (userData.role.name !== "admin") {
        const error = new Error(req.__("accessDenied"));
        error.statusCode = 401;
        next(error);
      }
      next();
    }
  }
};
