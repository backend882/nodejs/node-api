const t = require("../helpers/translation");
const bcrypt = require("bcryptjs");
const crypto = require("crypto");
const models = require("../models");
const Roles = models.role;
const Users = models.user;
const { Op } = require("sequelize");

exports.me = (req, res, next) => {
  const userLogin = req.userLogin;
  res.status(200).json({
    message: req.__("success.found", req.__("user")),
    data: userLogin,
  });
};

exports.createUser = async (req, res, next) => {
  let stat;
  let role;
  const token = crypto.randomBytes(20).toString("hex");

  const email = req.body.email;
  const password = req.body.password;
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const userRole = req.body.role;
  const status = "1";
  const verified = "1";

  role = userRole;
  stat = status;

  const hashPassword = await bcrypt.hash(
    password,
    Number(process.env.PASS_HASH)
  );

  const createUser = await Users.create({
    firstName: firstName,
    lastName: lastName,
    email: email,
    password: hashPassword,
    roleId: role,
    status: stat,
    verified: verified,
    emailToken: token,
  });
  let msg = req.__("success.create", req.__("user"));
  if (createUser) {
    res.status(201).json({
      message: msg,
      data: createUser,
    });
  } else {
    const error = new Error(req.__("error.fail", req.__("create")));
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    return next(error);
  }
};

exports.update = async (req, res, next) => {
  const userId = req.params.userId;

  const user = await Users.findOne({ where: { id: userId } });
  if (user) {
    user.firstName = req.body.firstName;
    user.lastName = req.body.lastName;
    user.email = req.body.email;
    user.phone = req.body.phone;
    user.roleId = req.body.role;
    if (req.body.password) {
      const hashPassword = await bcrypt.hash(
        req.body.password,
        Number(process.env.PASS_HASH)
      );
      user.password = hashPassword;
    }
    const updateUser = await user.save();
    if (updateUser) {
      res.status(200).json({
        message: req.__("success.update", req.__("user")),
        data: user,
      });
    } else {
      const error = new Error(req.__("error.fail", req.__("update")));
      if (!error.statusCode) {
        error.statusCode = 400;
      }
      return next(error);
    }
  } else {
    const error = new Error(req.__("error.notFound", req.__("user")));
    if (!error.statusCode) {
      error.statusCode = 404;
    }
    return next(error);
  }
};

exports.delete = async (req, res, next) => {
  const userId = req.params.userId;
  if (userId === "1") {
    const error = new Error(req.__("error.notFound", req.__("user")));
    if (!error.statusCode) {
      error.statusCode = 404;
    }
  }
  const user = await Users.findOne({ where: { id: userId } });
  if (user) {
    const deleteUser = await Users.destroy({ where: { id: userId } });

    if (deleteUser) {
      res.status(200).json({
        message: req.__("success.delete", req.__("user")),
        data: userId,
      });
    } else {
      const error = new Error(req.__("error.fail", req.__("delete")));
      if (!error.statusCode) {
        error.statusCode = 400;
      }
      return next(error);
    }
  } else {
    const error = new Error(req.__("error.notFound", req.__("user")));
    if (!error.statusCode) {
      error.statusCode = 404;
    }
    return next(error);
  }
};

exports.updateProfile = async (req, res, next) => {
  const userId = req.userId;

  const user = await Users.findOne({ where: { id: userId } });
  if (user) {
    user.firstName = req.body.firstName;
    user.lastName = req.body.lastName;
    user.email = req.body.email;
    user.phone = req.body.phone;
    const updateProfile = await user.save();
    if (updateProfile) {
      res.status(200).json({
        message: req.__("success.update", req.__("profile")),
        data: user,
      });
    } else {
      const error = new Error(req.__("error.fail", req.__("resetPassword")));
      if (!error.statusCode) {
        error.statusCode = 400;
      }
      return next(error);
    }
  } else {
    const error = new Error(req.__("error.notFound", req.__("user")));
    if (!error.statusCode) {
      error.statusCode = 404;
    }
    return next(error);
  }
};

exports.updatePassword = async (req, res, next) => {
  const userId = req.userId;
  const oldPassword = req.body.oldPassword;
  const password = req.body.newPassword;
  const confirmPassword = req.body.confirmPassword;
  const hashPassword = await bcrypt.hash(
    password,
    Number(process.env.PASS_HASH)
  );
  const user = await Users.findOne({ where: { id: userId } });
  if (user) {
    const isEqual = await bcrypt.compare(oldPassword, user.password);
    if (!isEqual) {
      const error = new Error(
        req.__("error.form.wrong", req.__("oldPassword"))
      );
      error.statusCode = 400;
      return next(error);
    }
    if (confirmPassword !== password) {
      const error = new Error(
        req.__("error.form.notMatch", req.__("confirmPassword"))
      );
      error.statusCode = 400;
      return next(error);
    }
    user.password = hashPassword;
    const updatePassword = await user.save();
    if (updatePassword) {
      res.status(200).json({
        message: req.__("success.update", req.__("password")),
        data: user,
      });
    } else {
      const error = new Error(req.__("error.fail", req.__("resetPassword")));
      if (!error.statusCode) {
        error.statusCode = 400;
      }
      return next(error);
    }
  } else {
    const error = new Error(req.__("error.notFound", req.__("user")));
    if (!error.statusCode) {
      error.statusCode = 404;
    }
    return next(error);
  }
};

exports.activate = async (req, res, next) => {
  const userId = req.params.userId;
  if (userId === "1") {
    const error = new Error(req.__("error.notFound", req.__("user")));
    if (!error.statusCode) {
      error.statusCode = 404;
    }
  }
  const user = await Users.findOne({ where: { id: userId } });
  if (user) {
    if (user.verified === 0) {
      user.verified = 1;
    }
    if (user.status === 0) {
      user.status = 1;
    } else {
      user.status = 0;
    }
    const updateUser = await user.save();
    if (updateUser) {
      res.status(200).json({
        message: req.__("success.update", req.__("user")),
        data: user,
      });
    } else {
      const error = new Error(req.__("error.fail", req.__("update")));
      if (!error.statusCode) {
        error.statusCode = 400;
      }
      return next(error);
    }
  } else {
    const error = new Error(req.__("error.notFound", req.__("user")));
    if (!error.statusCode) {
      error.statusCode = 404;
    }
    return next(error);
  }
};

exports.getAllUsers = async (req, res, next) => {
  const { all, offset, limit, search, orderBy, sort } = req.query;
  let total = 0;
  let start = 0;
  let limits = 10;
  let searchs = {};
  let tipe = { status: 1, verified: 1 };
  let order_by = "id";
  let sort_by = "DESC";

  if (offset) {
    start = offset;
  }
  if (limit) {
    limits = limit;
  }

  if (all && all === "all") {
    tipe = {};
  }
  if (orderBy) {
    order_by = orderBy;
  }
  if (sort) {
    sort_by = sort;
  }

  if (search) {
    searchs = {
      [Op.or]: [
        { firstName: { [Op.substring]: search } },
        { lastName: { [Op.substring]: search } },
        { email: { [Op.substring]: search } },
      ],
    };
  }

  const totalUser = await Users.findAll({ where: { ...searchs, ...tipe } });
  if (totalUser) {
    total = totalUser.length;
  }

  Users.findAll({
    where: { ...searchs, ...tipe },
    offset: Number(start),
    limit: Number(limits),
    order: [[order_by, sort_by]],
    include: Roles,
  }).then((data) => {
    res.status(200).json({
      message: req.__("success.found", req.__("user")),
      total: total,
      data: data,
    });
  });
};

exports.getUser = async (req, res, next) => {
  const userId = req.params.userId;
  if (userId === "1") {
    const error = new Error(req.__("error.notFound", req.__("user")));
    if (!error.statusCode) {
      error.statusCode = 404;
    }
    return next(error);
  }
  const user = await Users.findOne({ include: Roles, where: { id: userId } });
  if (user) {
    res.status(200).json({
      message: req.__("success.found", req.__("user")),
      data: user,
    });
  } else {
    const error = new Error(req.__("error.notFound", req.__("user")));
    if (!error.statusCode) {
      error.statusCode = 404;
    }
    return next(error);
  }
};
