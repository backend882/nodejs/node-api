const models = require("../models");
const Roles = models.role;
const { Op } = require("sequelize");

exports.getAllRoles = async (req, res, next) => {
  const { offset, limit, search, orderBy, sort } = req.query;
  let total = 0;
  let start = 0;
  let limits = 10;
  let searchs = {};
  let order_by = "id";
  let sort_by = "DESC";

  if (offset) {
    start = offset;
  }
  if (limit) {
    limits = limit;
  }

  if (orderBy) {
    order_by = orderBy;
  }
  if (sort) {
    sort_by = sort;
  }

  if (search) {
    searchs = {
      [Op.or]: [{ name: { [Op.substring]: search } }],
    };
  }

  const totalRole = await Roles.findAll({ where: { ...searchs } });
  if (totalRole) {
    total = totalRole.length;
  }

  Roles.findAll({
    where: { ...searchs },
    offset: Number(start),
    limit: Number(limits),
    order: [[order_by, sort_by]],
  }).then((data) => {
    res.status(200).json({
      message: req.__("success.found", req.__("data")),
      total: total,
      data: data,
    });
  });
};

exports.create = async (req, res, next) => {
  const name = req.body.name;

  const createRole = await Roles.create({
    name: name,
  });
  let msg = req.__("success.create", req.__("role"));
  if (createRole) {
    res.status(201).json({
      message: msg,
      data: createRole,
    });
  } else {
    const error = new Error(req.__("error.fail", req.__("create")));
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    return next(error);
  }
};

exports.update = async (req, res, next) => {
  const roleId = req.params.roleId;
  if (roleId === "1") {
    const error = new Error(req.__("error.notFound", req.__("role")));
    if (!error.statusCode) {
      error.statusCode = 404;
    }
    return next(error);
  }
  const role = await Roles.findOne({ where: { id: roleId } });
  if (role) {
    role.name = req.body.name;
    const updateRole = await role.save();
    if (updateRole) {
      res.status(200).json({
        message: req.__("success.update", req.__("role")),
        data: role,
      });
    } else {
      const error = new Error(req.__("error.fail", req.__("delete")));
      if (!error.statusCode) {
        error.statusCode = 400;
      }
      return next(error);
    }
  } else {
    const error = new Error(req.__("error.notFound", req.__("data")));
    if (!error.statusCode) {
      error.statusCode = 404;
    }
    return next(error);
  }
};

exports.delete = async (req, res, next) => {
  const roleId = req.params.roleId;
  if (roleId === "1") {
    const error = new Error(req.__("error.notFound", req.__("role")));
    if (!error.statusCode) {
      error.statusCode = 404;
    }
    return next(error);
  }
  const role = await Roles.findOne({ where: { id: roleId } });
  if (role) {
    const deleteRole = await Roles.destroy({ where: { id: roleId } });

    if (deleteRole) {
      res.status(200).json({
        message: req.__("success.delete", req.__("role")),
        data: roleId,
      });
    } else {
      const error = new Error(req.__("error.fail", req.__("delete")));
      if (!error.statusCode) {
        error.statusCode = 400;
      }
      return next(error);
    }
  } else {
    const error = new Error(req.__("error.notFound", req.__("role")));
    if (!error.statusCode) {
      error.statusCode = 404;
    }
    return next(error);
  }
};

exports.getRole = async (req, res, next) => {
  const roleId = req.params.roleId;

  const role = await Roles.findOne({ where: { id: roleId } });
  if (role) {
    res.status(200).json({
      message: req.__("success.found", req.__("data")),
      data: role,
    });
  } else {
    const error = new Error(req.__("error.notFound", req.__("data")));
    if (!error.statusCode) {
      error.statusCode = 404;
    }
    return next(error);
  }
};
