require("dotenv").config();
const verifyEmail = require("../mail/registerVerification");
const forgotEmail = require("../mail/forgotPassword");
const randtoken = require("rand-token");
const crypto = require("crypto");
const models = require("../models");
const Roles = models.role;
const Users = models.user;
const Option = models.option;

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

exports.signin = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  let loadedUser;
  let refreshToken;
  Users.findOne({ where: { email: email }, include: Roles })
    .then(async (user) => {
      if (!user) {
        const error = new Error(req.__("error.notFound", req.__("email")));
        error.statusCode = 400;
        throw error;
      }
      loadedUser = user;
      return bcrypt.compare(password, user.password);
    })
    .then((isEqual) => {
      if (!isEqual) {
        const error = new Error(req.__("error.form.wrong", req.__("password")));
        error.statusCode = 400;
        throw error;
      }
      if (loadedUser.status !== 1) {
        const error = new Error(req.__("error.notActive", req.__("user")));
        error.statusCode = 400;
        throw error;
      }
      if (loadedUser.verified !== 1) {
        const error = new Error(req.__("unverified"));
        error.statusCode = 400;
        error.data = { resend: true };
        throw error;
      }

      refreshToken = randtoken.uid(256);
      loadedUser.refreshToken = refreshToken;
      return loadedUser.save();
    })
    .then(() => {
      const token = generateJWTToken(loadedUser);
      res.status(200).json({
        message: req.__("success.*", req.__("login")),
        data: {
          userId: loadedUser.id.toString(),
          userRole: loadedUser.role.name.toString(),
          token: token,
          refreshToken: refreshToken,
        },
      });
    })
    .catch((error) => {
      if (!error.statusCode) {
        error.statusCode = 500;
      }
      next(error);
    });
};

exports.refreshToken = async (req, res, next) => {
  const token = req.body.token;
  let refreshToken;
  let loadedUser;
  Users.findOne({ where: { refreshToken: token }, include: Roles })
    .then((user) => {
      if (!user) {
        const error = new Error(req.__("error.notFound", req.__("user")));
        error.statusCode = 400;
        throw error;
      }
      if (user.status !== 1) {
        const error = new Error(req.__("error.notActive", req.__("user")));
        error.statusCode = 400;
        return next(error);
      }
      if (user.verified !== 1) {
        const error = new Error(req.__("unverified"));
        error.statusCode = 400;
        error.data = { resend: true };
        return next(error);
      }
      loadedUser = user;
      refreshToken = randtoken.uid(256);
      loadedUser.refreshToken = refreshToken;
      return user.save();
    })
    .then(() => {
      const newToken = generateJWTToken(loadedUser);
      res.status(200).json({
        message: req.__("success.*", req.__("refreshToken")),
        data: {
          userId: loadedUser.id.toString(),
          userRole: loadedUser.role.name.toString(),
          token: newToken,
          refreshToken: refreshToken,
        },
      });
    })
    .catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.signup = async (req, res, err) => {
  let stat;
  let role;
  let verified;
  const token = crypto.randomBytes(20).toString("hex");

  const email = req.body.email;
  const password = req.body.password;
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const userRole = req.body.role;
  const status = req.body.status;

  role = userRole;
  stat = status;
  verified = "1";

  const hashPassword = await bcrypt.hash(
    password,
    Number(process.env.PASS_HASH)
  );
  if (!userRole) {
    role = await getDefaultRole();
  }
  if (!status) {
    stat = await getDefaultStatus();
  }
  verfiedStat = await virifiedUser();
  if (verfiedStat === true) {
    verified = "0";
  }
  const createUser = await Users.create({
    firstName: firstName,
    lastName: lastName,
    email: email,
    password: hashPassword,
    roleId: role,
    status: stat,
    verified: verified,
    emailToken: token,
  });
  let msg = req.__("success.create", req.__("user"));
  if (createUser) {
    if (verfiedStat) {
      msg = req.__("notification.emailVerification");
      verifyEmail(createUser);
    }
    res.status(201).json({
      message: msg,
      data: createUser,
    });
  } else {
    const error = new Error(req.__("error.fail", req.__("registration")));
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    return next(error);
  }
};

exports.verifyEmail = (req, res, next) => {
  const newToken = crypto.randomBytes(20).toString("hex");
  const emailToken = req.body.token;
  Users.findOne({ where: { emailToken: emailToken } })
    .then(async (user) => {
      if (!user) {
        const error = new Error(req.__("error.notFound", req.__("user")));
        error.statusCode = 400;
        return next(error);
      }
      user.emailToken = newToken;
      user.verified = "1";
      const update = await user.save();
      if (update) {
        res.status(200).json({
          message: req.__("success.verify"),
          data: update,
        });
      } else {
        const error = new Error(req.__("error.fail", req.__("update")));
        if (!error.statusCode) {
          error.statusCode = 400;
        }
        return next(error);
      }
    })
    .catch((err) => {
      const error = new Error(err);
      if (!error.statusCode) {
        error.statusCode = 500;
      }
      return next(error);
    });
};

exports.forgotPassword = (req, res, next) => {
  const newToken = crypto.randomBytes(20).toString("hex");
  const email = req.body.email;
  Users.findOne({ where: { email: email } })
    .then(async (user) => {
      if (!user) {
        const error = new Error(req.__("error.notFound", req.__("user")));
        error.statusCode = 400;
        return next(error);
      }
      if (user.verified !== 1) {
        const error = new Error(req.__("unverified"));
        if (!error.statusCode) {
          error.statusCode = 400;
        }
        return next(error);
      }
      user.emailToken = newToken;
      const update = await user.save();
      if (update) {
        const send = await forgotEmail(update);
        if (send) {
          res.status(200).json({
            message: req.__("notification.emailForgotPassword"),
            data: { email: user.email },
          });
        } else {
          const error = new Error(req.__("error.fail", req.__("sendEmail")));
          if (!error.statusCode) {
            error.statusCode = 400;
          }
          return next(error);
        }
      } else {
        const error = new Error(req.__("error.fail", req.__("update")));
        if (!error.statusCode) {
          error.statusCode = 400;
        }
        return next(error);
      }
    })
    .catch((err) => {
      const error = new Error(err);
      if (!error.statusCode) {
        error.statusCode = 500;
      }
      return next(error);
    });
};

exports.resetPassword = async (req, res, next) => {
  const newToken = crypto.randomBytes(20).toString("hex");
  const emailToken = req.body.token;
  const password = req.body.password;
  const confirmPassword = req.body.confirmPassword;
  if (password !== confirmPassword) {
    const error = new Error(
      req.__("error.form.notMatch", req.__("confirmPassword"))
    );
    if (!error.statusCode) {
      error.statusCode = 400;
    }
    return next(error);
  }

  const hashPassword = await bcrypt.hash(
    password,
    Number(process.env.PASS_HASH)
  );
  Users.findOne({ where: { emailToken: emailToken } })
    .then(async (user) => {
      if (!user) {
        const error = new Error(req.__("error.notFound", req.__("user")));
        error.statusCode = 400;
        return next(error);
      }
      user.emailToken = newToken;
      user.password = hashPassword;
      const update = await user.save();
      if (update) {
        res.status(200).json({
          message: req.__("success.update", req.__("resetPassword")),
          data: { email: user.email },
        });
      } else {
        const error = new Error(req.__("error.fail", req.__("resetPassword")));
        if (!error.statusCode) {
          error.statusCode = 400;
        }
        return next(error);
      }
    })
    .catch((err) => {
      const error = new Error(err);
      if (!error.statusCode) {
        error.statusCode = 500;
      }
      return next(error);
    });
};

const getDefaultRole = async () => {
  let nameRole;
  const defaultRole = await Option.findOne({ where: { key: "default_role" } });
  if (defaultRole === null) {
    return generateDefaultRole();
  }
  nameRole = defaultRole.value;
  const roleData = await Roles.findOne({ where: { name: nameRole } });
  if (roleData === null) {
    return generateDefaultRole();
  }
  return roleData.id;
};

const getDefaultStatus = async () => {
  let stat = "0";
  const defaultStatus = await Option.findOne({
    where: { key: "auto_active_registration" },
  });
  if (defaultStatus !== null && defaultStatus.value === "1") {
    stat = "1";
  }
  return stat;
};

const generateDefaultRole = async () => {
  const roleSave = await Roles.create({ key: "default_role", value: "user" });
  return roleSave.id;
};

const virifiedUser = async () => {
  let verifiy = false;
  const defaultVerify = await Option.findOne({
    where: { key: "verify_email" },
  });
  if (defaultVerify !== null && defaultVerify.value === "1") {
    verifiy = true;
  }
  return verifiy;
};

const generateJWTToken = (user) => {
  return (token = jwt.sign(
    {
      email: user.email,
      userId: user.id.toString(),
      userRole: user.role.name.toString(),
    },
    process.env.PRIVATE_KEY,
    { expiresIn: "1w" }
  ));
};
