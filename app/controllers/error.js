const t = require("../helpers/translation");

exports.get404 = (req, res, next) => {
  const error = new Error(req.__("error.notFound", req.__("url")));
  error.statusCode = 404;
  throw error;
};
