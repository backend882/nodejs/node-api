const express = require("express");
const router = express.Router();
const roleController = require("../controllers/roles");
const { createRole } = require("../filters/validations/roles/create");
const { updateRole } = require("../filters/validations/roles/update");

const isAuth = require("../filters/isAuth");
const isAdmin = require("../filters/isAdmin");

router.get("/", isAuth, isAdmin, roleController.getAllRoles);
router.post("/", createRole, isAuth, isAdmin, roleController.create);
router.put("/:roleId", updateRole, isAuth, isAdmin, roleController.update);
router.patch("/:roleId", updateRole, isAuth, isAdmin, roleController.update);
router.get("/:roleId", isAuth, isAdmin, roleController.getRole);
router.delete("/:roleId", isAuth, isAdmin, roleController.delete);

module.exports = router;
