const express = require("express");
const router = express.Router();
const userController = require("../controllers/users");
const { updateUser } = require("../filters/validations/users/update");
const { createUser } = require("../filters/validations/users/create");

const isAuth = require("../filters/isAuth");
const isAdmin = require("../filters/isAdmin");

const {
  updateProfile,
} = require("../filters/validations/profile/updateProfile");
const {
  updatePassword,
} = require("../filters/validations/profile/updatePassword");

router.get("/", isAuth, userController.getAllUsers);
router.post("/", createUser, isAuth, isAdmin, userController.createUser);
router.get("/me", isAuth, userController.me);
router.get("/:userId", isAuth, userController.getUser);
router.delete("/:userId", isAuth, userController.delete);
router.put("/:userId", updateUser, isAuth, userController.update);
router.patch("/:userId", updateUser, isAuth, userController.update);
router.get("/activate/:userId", isAuth, userController.activate);
router.post(
  "/update-profile",
  isAuth,
  updateProfile,
  userController.updateProfile
);
router.post(
  "/update-password",
  isAuth,
  updatePassword,
  userController.updatePassword
);

module.exports = router;
