const express = require("express");
const router = express.Router();

const authController = require("../controllers/auth");

const isAuth = require("../filters/isAuth");
const registrationActive = require("../filters/activeRegistation");

const { signup } = require("../filters/validations/signup");
const { signin } = require("../filters/validations/signin");
const { verifyEmail } = require("../filters/validations/verifyEmail");
const { forgotPassword } = require("../filters/validations/forgotPassword");
const { resetPassword } = require("../filters/validations/resetPassword");
const { refreshToken } = require("../filters/validations/refreshToken");

router.post("/signin", signin, authController.signin);
router.post("/signup", registrationActive, signup, authController.signup);
router.post("/verify-email", verifyEmail, authController.verifyEmail);
router.post("/forgot-password", forgotPassword, authController.forgotPassword);
router.post("/reset-password", resetPassword, authController.resetPassword);
router.post(
  "/refresh-token",
  isAuth,
  refreshToken,
  authController.refreshToken
);

module.exports = router;
