const express = require("express");
const t = require("../helpers/translation");
const sendEmail = require("../configs/emails");
const router = express.Router();
router.get("/", (req, res, err) => {
  const msg = "index";
  res.status(200).json({ message: msg });
});

module.exports = router;
