"use strict";
const { Model } = require("sequelize");
const PROTECTED_ATTRIBUTES = [
  "password",
  "emailToken",
  "refreshToken",
  "createdAt",
  "updatedAt",
  "deletedAt",
];

module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    toJSON() {
      // hide protected fields
      let attributes = Object.assign({}, this.get());
      for (let a of PROTECTED_ATTRIBUTES) {
        delete attributes[a];
      }
      return attributes;
    }
    static associate(models) {
      user.belongsTo(models.role);
    }
  }
  user.init(
    {
      firstName: DataTypes.STRING,
      lastName: DataTypes.STRING,
      phone: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      roleId: DataTypes.INTEGER,
      status: DataTypes.INTEGER,
      verified: DataTypes.INTEGER,
      emailToken: DataTypes.STRING,
      refreshToken: DataTypes.TEXT,
    },
    {
      sequelize,
      modelName: "user",
      paranoid: true,
    }
  );
  return user;
};
