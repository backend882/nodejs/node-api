"use strict";
const { Model } = require("sequelize");
const PROTECTED_ATTRIBUTES = [
  "password",
  "emailToken",
  "refreshToken",
  "createdAt",
  "updatedAt",
  "deletedAt",
];

module.exports = (sequelize, DataTypes) => {
  class role extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    toJSON() {
      // hide protected fields
      let attributes = Object.assign({}, this.get());
      for (let a of PROTECTED_ATTRIBUTES) {
        delete attributes[a];
      }
      return attributes;
    }
    static associate(models) {
      role.hasMany(models.user);
    }
  }
  role.init(
    {
      name: DataTypes.STRING,
      access: DataTypes.TEXT,
    },
    {
      sequelize,
      modelName: "role",
      paranoid: true,
    }
  );
  return role;
};
