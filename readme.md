## Install

- npm install
- rename env-example to .env and configure it (database)
- run **npx sequelize-cli db:migrate**
- run **npx sequelize-cli db:seed:all**
