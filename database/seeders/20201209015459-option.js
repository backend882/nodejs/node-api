"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "options",
      [
        {
          key: "site_title",
          value: "Node App",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          key: "registration",
          value: "0",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          key: "default_role",
          value: "user",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          key: "verify_email",
          value: "0",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          key: "auto_active_registration",
          value: "0",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          key: "mail_driver",
          value: "smtp",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          key: "mail_host",
          value: "mailtrap.io",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          key: "mail_port",
          value: "2525",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          key: "mail_encryption",
          value: "tls",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          key: "mail_username",
          value: "f700301e3c5a99",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          key: "mail_password",
          value: "758f62b8c69b2c",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          key: "mail_from_address",
          value: "support@system.com",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          key: "mail_from_name",
          value: "support",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("options", null, {});
  },
};
