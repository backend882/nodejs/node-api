"use strict";
require("dotenv").config();
const bcrypt = require("bcryptjs");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return bcrypt
      .hash("admin", Number(process.env.PASS_HASH))
      .then((passwordhash) => {
        return queryInterface.bulkInsert(
          "users",
          [
            {
              firstName: "admin",
              lastName: "admin",
              email: "admin@gmail.com",
              password: passwordhash,
              roleId: "1",
              verified: "1",
              status: "1",
              createdAt: new Date(),
              updatedAt: new Date(),
            },
          ],

          {}
        );
      });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("users", null, {});
  },
};
